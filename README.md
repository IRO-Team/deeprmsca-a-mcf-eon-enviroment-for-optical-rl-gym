# optical-rl Gym; Multicore-env
This is a environment for multicore optical networks, compatible with optical-rl Gym with inter-core cross-talk calculator function and threshold included for a different multicore arrangment.

**Instalation**

For installation details of the optical-RL Gym framework, you can visit the repository:
https://github.com/carlosnatalino/optical-rl-gym

When the libraries are installed, the 'Env' files must be placed in the folder with the same name.

Then, to train in our environment, you can use the standard GYM procedure such as:

```python
# define the parameters of the RMSCA (MCF) environment
env_args = dict(topology=topology, seed=1, allow_rejection=False, 
                load=500, episode_length=500)
# create the environment
env = gym.make('RMSCA-v0', **env_args)
# create the agent
agent = TRPO(MlpPolicy, env)
# run 10k learning timesteps
agent.learn(total_timesteps=1000000)
```

However, for a quick test, an example in Google Colab is attached
https://colab.research.google.com/drive/1ac7fQfy8vlt081c7KxNaC1ToqtOCCorp?authuser=1#scrollTo=VeWKvJpfoKCn

## List of agent's hyperparameters 
TRPO Agent
https://stable-baselines.readthedocs.io/en/master/modules/trpo.html#parameters

PPO2 Agent
https://stable-baselines.readthedocs.io/en/master/modules/ppo2.html#parameters

A2C Agent
https://stable-baselines.readthedocs.io/en/master/modules/a2c.html#parameters

ACKTR Agent
https://stable-baselines.readthedocs.io/en/master/modules/acktr.html#parameters


Original contribution: 

1. C. Natalino and P. Monti, **The Optical RL-Gym: an open-source toolkit for applying reinforcement learning in optical networks**, ICTON, July, 2020: introduces the toolkit and presents a two use cases.

```
@inproceedings{optical-rl-gym,
  title = {The {Optical RL-Gym}: an open-source toolkit for applying reinforcement learning in optical networks},
  author = {Carlos Natalino and Paolo Monti},
  booktitle = {International Conference on Transparent Optical Networks (ICTON)},
  year = {2020},
  location = {Bari, Italy},
  month = {July},
  pages = {Mo.C1.1},
  url = {https://github.com/carlosnatalino/optical-rl-gym}
}
```
